Attribute VB_Name = "INIUtilities"
' INI 文件读写模块

Option Explicit

Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
' 参数类型及说明
' lpApplicationName：String，欲在其中查找条目的小节名称。这个字串不区分大小写。如设为 vbNullString，
'                    就在 lpReturnedString 缓冲区内装载这个 ini 文件所有小节的列表
' lpKeyName：        String，欲获取的项名或条目名。这个字串不区分大小写。
'                    如设为 vbNullString，就在 lpReturnedString 缓冲区内装载指定小节所有项的列表
' lpDefault：        String，指定的条目没有找到时返回的默认值。可设为空（""）
' lpReturnedString： String，指定一个字串缓冲区，长度至少为 nSize
' nSize：            Long，指定装载到 lpReturnedString 缓冲区的最大字符数量
' lpFileName：       String，初始化文件的名字。如没有指定一个完整路径名，Windows 就在 Windows 目录中查找文件

Private m_strINIFilename As String

Public Sub InitINI(strINIFilename)
    m_strINIFilename = strINIFilename
End Sub

' 删除一个参数或小节
Public Sub DeleteINISet(strSection As String, strKeyWord As String)

    Dim lngReturn As Long
    
    lngReturn = WritePrivateProfileString(strSection, strKeyWord, vbNullString, m_strINIFilename)
    
    If lngReturn = 0 Then
        ' 失败
    Else
        ' 成功
    End If
    
End Sub

' 读取一个参数
Public Function GetINISet(strSection As String, strKeyWord As String, Optional varDefault As Variant) As Variant

    Dim strReturn As String
    Dim lngSize As Long
    Dim lngReturn As Long
    
    lngSize = 250
    strReturn = Space(lngSize)
    
    If IsMissing(varDefault) Then varDefault = ""
    
    lngReturn = GetPrivateProfileString(strSection, strKeyWord, varDefault, strReturn, lngSize, m_strINIFilename)
    
    If lngSize = -1 Then ' 缓冲区装不下读出的内容
        lngSize = 2048
        strReturn = Space(lngSize)
        lngReturn = GetPrivateProfileString(strSection, strKeyWord, varDefault, strReturn, lngSize, m_strINIFilename)
        strReturn = Left$(strReturn, lngReturn)
    ElseIf lngSize = -2 Then ' 小节不存在，或者关键值不存在
        strReturn = ""
    Else
        strReturn = StrConv(LeftB$(StrConv(strReturn, vbFromUnicode), lngReturn), vbUnicode)
    End If
    
    GetINISet = strReturn
    
End Function

' 设置一个参数
Public Sub SaveINISet(strSection As String, strKeyWord As String, varValue As Variant)

    Dim lngReturn As Long
    
    lngReturn = WritePrivateProfileString(strSection, strKeyWord, CStr(varValue), m_strINIFilename)
    
    If lngReturn = 0 Then
        ' 失败
    Else
        ' 成功
    End If
    
End Sub

