Attribute VB_Name = "FileUtilities"
' 读写文件模块

Option Explicit

Public Const INVALID_HANDLE_VALUE = -1

Public Const FILE_ATTRIBUTE_READONLY = &H1
Public Const FILE_ATTRIBUTE_HIDDEN = &H2
Public Const FILE_ATTRIBUTE_SYSTEM = &H4
Public Const FILE_ATTRIBUTE_DIRECTORY = &H10
Public Const FILE_ATTRIBUTE_ARCHIVE = &H20
Public Const FILE_ATTRIBUTE_NORMAL = &H80
Public Const FILE_ATTRIBUTE_TEMPORARY = &H100
Public Const FILE_ATTRIBUTE_COMPRESSED = &H800

Public Const MAX_PATH = 260

Public Const CSIDL_DESKTOP = &H0                     ' <desktop>
Public Const CSIDL_INTERNET = &H1                    ' Internet Explorer (icon on desktop)
Public Const CSIDL_PROGRAMS = &H2                    ' Start Menu\Programs
Public Const CSIDL_CONTROLS = &H3                    ' My Computer\Control Panel
Public Const CSIDL_PRINTERS = &H4                    ' My Computer\Printers
Public Const CSIDL_PERSONAL = &H5                    ' My Documents
Public Const CSIDL_FAVORITES = &H6                   ' <user name>\Favorites
Public Const CSIDL_STARTUP = &H7                     ' Start Menu\Programs\Startup
Public Const CSIDL_RECENT = &H8                      ' <user name>\Recent
Public Const CSIDL_SENDTO = &H9                      ' <user name>\SendTo
Public Const CSIDL_BITBUCKET = &HA                   ' <desktop>\Recycle Bin
Public Const CSIDL_STARTMENU = &HB                   ' <user name>\Start Menu
Public Const CSIDL_MYDOCUMENTS = &HC                 ' logical "My Documents" desktop icon
Public Const CSIDL_MYMUSIC = &HD                     ' "My Music" folder
Public Const CSIDL_MYVIDEO = &HE                     ' "My Videos" folder
Public Const CSIDL_DESKTOPDIRECTORY = &H10           ' <user name>\Desktop
Public Const CSIDL_DRIVES = &H11                     ' My Computer
Public Const CSIDL_NETWORK = &H12                    ' Network Neighborhood (My Network Places)
Public Const CSIDL_NETHOOD = &H13                    ' <user name>\nethood
Public Const CSIDL_FONTS = &H14                      ' windows\fonts
Public Const CSIDL_TEMPLATES = &H15
Public Const CSIDL_COMMON_STARTMENU = &H16           ' All Users\Start Menu
Public Const CSIDL_COMMON_PROGRAMS = &H17            ' All Users\Start Menu\Programs
Public Const CSIDL_COMMON_STARTUP = &H18             ' All Users\Startup
Public Const CSIDL_COMMON_DESKTOPDIRECTORY = &H19    ' All Users\Desktop
Public Const CSIDL_APPDATA = &H1A                    ' <user name>\Application Data
Public Const CSIDL_PRINTHOOD = &H1B                  ' <user name>\PrintHood
Public Const CSIDL_INTERNET_CACHE = &H20
Public Const CSIDL_COOKIES = &H21
Public Const CSIDL_HISTORY = &H22
Public Const CSIDL_COMMON_APPDATA = &H23             ' All Users\Application Data
Public Const CSIDL_WINDOWS = &H24                    ' GetWindowsDirectory()
Public Const CSIDL_SYSTEM = &H25                     ' GetSystemDirectory()
Public Const CSIDL_PROGRAM_FILES = &H26              ' C:\Program Files
Public Const CSIDL_MYPICTURES = &H27                 ' C:\Program Files\My Pictures

Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Type FILETIME
    dwLowDateTime       As Long
    dwHighDateTime      As Long
End Type

Public Type WIN32_FIND_DATA
    dwFileAttributes    As Long
    ftCreationTime      As FILETIME
    ftLastAccessTime    As FILETIME
    ftLastWriteTime     As FILETIME
    nFileSizeHigh       As Long
    nFileSizeLow        As Long
    dwReserved0         As Long
    dwReserved1         As Long
    cFileName           As String * MAX_PATH
    cAlternateFileName  As String * 14
End Type

Public Enum WriteMode
    Overwrite = 0
    Appand = 1
End Enum

Public Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
Public Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long
Public Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
Public Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Public Declare Function SHGetSpecialFolderLocation Lib "Shell32" (ByVal hwndOwner As Long, ByVal nFolder As Integer, ppidl As Long) As Long
Public Declare Function SHGetPathFromIDList Lib "Shell32" Alias "SHGetPathFromIDListA" (ByVal pidl As Long, ByVal szPath As String) As Long
Public Declare Function GetShortPathName Lib "kernel32" Alias "GetShortPathNameA" (ByVal lpszLongPath As String, ByVal lpszShortPath As String, ByVal cchBuffer As Long) As Long
Public Declare Function GetFileAttributes Lib "kernel32" Alias "GetFileAttributesA" (ByVal lpFileName As String) As Long

' 创建快捷方式
' strLinkFile -快捷方式文件路径
' strTargetFile -快捷方式指向的目标文件路径
' strIconFile -快捷方式使用的图标所在的文件路径
' lngIconIndex -快捷方式图标在文件中的索引
' strArguments -启动目标程序的参数
' strWorkDirectory -目标程序工作目录
' strHotkey -访问快捷方式的热键
' lngWindowStyle -启动目标程序的窗口样式
' strDescription -快捷方式的描述
' 返回值 -创建快捷方式是否成功
Public Function CreateShortcut(strLinkFile As String, strTargetFile As String, Optional strIconFile As String = "", Optional lngIconIndex As Long = 0, Optional strArguments As String = "", Optional strWorkDirectory As String = "", Optional strHotkey As String = "", Optional lngWindowStyle As Long = vbNormal, Optional strDescription As String = "") As Boolean

    Dim wsh As New WshShell
    Dim objShellLink As WshShortcut

    On Error GoTo PROC_ERR

    Set objShellLink = wsh.CreateShortcut(strLinkFile)
    objShellLink.TargetPath = strTargetFile
    objShellLink.Arguments = strArguments
    objShellLink.IconLocation = strIconFile & "," & lngIconIndex
    objShellLink.Description = strDescription
    objShellLink.WorkingDirectory = strWorkDirectory
    objShellLink.Hotkey = strHotkey
    objShellLink.WindowStyle = lngWindowStyle
    objShellLink.Save

    CreateShortcut = True

PROC_EXIT:
    Exit Function

PROC_ERR:
    CreateShortcut = False
    GoTo PROC_EXIT

End Function

' 判断文件是否存在
Public Function ExistFile(strFullFileName As String) As Boolean

    Dim lngReturnValue As Long
    
    lngReturnValue = GetFileAttributes(strFullFileName)
    
    If lngReturnValue = -1 Then
        ExistFile = False
    Else
        ExistFile = True
    End If
    
End Function

' 从文件路径分离出驱动器或主机名
Public Function ExtractDrive(strFilename As String) As String

    If Mid(strFilename, 2, 1) = ":" Then
        ExtractDrive = Left(strFilename, 2)
        Exit Function
    ElseIf Left(strFilename, 2) <> "\\" Then
        Exit Function
    End If
    
    Dim lngPos As Long
    
    lngPos = InStr(3, strFilename, "\")
    If lngPos > 0 Then
        ExtractDrive = Left(strFilename, lngPos - 1)
    End If
    
End Function

' 从文件全路径分离出目录
' strFilename - 文件全路径
' 返回值 - 文件所在的目录字符串
Public Function ExtractDirectory(strFilename As String, Optional blnEndWithBackSlash As Boolean = True) As String

    Dim lngPos As Long
    
    lngPos = InStrRev(strFilename, "\")
    
    If lngPos > 0 Then
        If blnEndWithBackSlash Then
            ExtractDirectory = Left(strFilename, lngPos)
        Else
            ExtractDirectory = Left(strFilename, lngPos - 1)
        End If
    End If
    
    If ExtractDirectory = "\" And Not blnEndWithBackSlash Or ExtractDirectory = "\\" Then
        ExtractDirectory = ""
    End If
    
End Function

' 取得文件扩展名
Public Function ExtractFileExtendName(strFilename As String) As String

    Dim lngPos As Long
    
    lngPos = InStrRev(ExtractFilename(strFilename), ".")
    
    If lngPos > 0 Then
        ExtractFileExtendName = Mid(strFilename, lngPos + 1)
    End If
    
End Function

' 获取文件属性
Public Function GetFileInfo(strFilename) As WIN32_FIND_DATA

    Dim Win32FindData As WIN32_FIND_DATA
    Dim lngHandle As Long
    
    lngHandle = FindFirstFile(strFilename, Win32FindData)
    If lngHandle = INVALID_HANDLE_VALUE Then
        GetFileInfo.cFileName = "" ' 如果取文件属性失败，返回空文件名
    Else
        GetFileInfo = Win32FindData
    End If
    
    ' 关闭 FindFirstFile 打开的句柄
    Call FindClose(lngHandle)
    
End Function

' 从文件全路径分离出文件名
' strFilename - 文件全路径
' 返回值 - 文件名及扩展名（不包含目录）
Public Function ExtractFilename(strFilename As String) As String

    Dim lngPos As Long
    
    lngPos = InStrRev(strFilename, "\")
    
    If lngPos > 0 Then
        ExtractFilename = Mid(strFilename, lngPos + 1)
    Else
        ExtractFilename = strFilename
    End If
    
End Function

' 从命令行参数分离出各个文件名
' strCommand - 命令行字符串
' 返回值 - 命令行各参数字符串数组，包含命令行可执行文件本身
Public Function GetFilenamesFromCommandLine(strCommand As String) As String()

    Const c_strTemp As String = "??"
    Dim strFilenames() As String
    
    If InStr(strCommand, " ") > 0 Then ' 带空格，有可能是单个或多个文件名
    
        If InStr(strCommand, """") > 0 Then ' 带引号，有可能是单个或多个文件名
            
            strCommand = Replace(strCommand, " """, c_strTemp) ' 将引号连同前后的空格替换为临时分隔符
            strCommand = Replace(strCommand, """ ", c_strTemp)
            strCommand = Replace(strCommand, """", "") ' 去除其余（主要是头尾）的引号
            strFilenames = Split(strCommand, c_strTemp) ' 临时分隔符分隔的即为各个文件名
            
        Else ' 不带引号，空格分隔的即为各个文件名
            
            strFilenames = Split(strCommand, " ")
        
        End If
        
    Else ' 不带空格，只可能是单个文件名
        
        ReDim strFilenames(0) As String
        strFilenames(0) = strCommand
        
    End If
    
    ' 返回结果
    GetFilenamesFromCommandLine = strFilenames
    
End Function

' 获取特殊目录
Public Function GetSpecialFolder(iFolderID As Long, Optional bEndWithBackSlash As Boolean = True)

    Dim iRetVal As Long
    Dim IDList As Long
    Dim sFolder As String
    
    iRetVal = SHGetSpecialFolderLocation(0, iFolderID, IDList)
    
    If iRetVal = 0 Then
    
        sFolder = Space(MAX_PATH)
        
        iRetVal = SHGetPathFromIDList(IDList, sFolder)
        
        GetSpecialFolder = Replace(Trim(sFolder), Chr(0), "")
        
        If bEndWithBackSlash Then
            GetSpecialFolder = GetSpecialFolder + "\"
        End If
        
    End If
    
End Function

' 计算两个路径的相对路径
Public Function MakeRelativePath(strPath As String, strRelativePath As String) As String

    If strRelativePath = "" Then
        MakeRelativePath = strPath
        Exit Function
    ElseIf ExtractDrive(strPath) <> ExtractDrive(strRelativePath) Then
        MakeRelativePath = strPath
        Exit Function
    End If
    
    Dim strParentDirectory As String
    
    If Right(strRelativePath, 1) = "\" Then
        strParentDirectory = Left(strRelativePath, Len(strRelativePath) - 1)
    Else
        strParentDirectory = strRelativePath
    End If
    
    Dim lngPos As Long
    
    While strParentDirectory <> ""
        lngPos = InStr(strPath, strParentDirectory)
        If lngPos = 1 Then
            If Mid(strPath, Len(strParentDirectory) + 1, 1) = "\" Then
                MakeRelativePath = MakeRelativePath & Mid(strPath, Len(strParentDirectory) + 2)
                Exit Function
            End If
        End If
        MakeRelativePath = MakeRelativePath & "..\"
        strParentDirectory = ExtractDirectory(strParentDirectory, False)
    Wend
    
End Function

' 从相对路径计算绝对路径
Public Function ResolveRelativePath(strPath As String, strRelativePath As String) As String

    If Mid(strPath, 2, 1) = ":" Or Left(strPath, 2) = "\\" Then
        ResolveRelativePath = strPath
        Exit Function
    End If
    
    ResolveRelativePath = strRelativePath
    
    If Right(ResolveRelativePath, 1) <> "\" Then
        ResolveRelativePath = ResolveRelativePath & "\"
    End If
    
    ResolveRelativePath = ResolveRelativePath & strPath
    
End Function

' 创建目录树，如果创建某级目录失败，则保留已创建的目录，返回失败
' strDirectory - 目录字符串
' 返回值 - 创建目录树是否成功
Public Function MakeDirectoryTree(strDirectory As String) As Boolean

    Dim strDirectorys() As String
    Dim strTemp As String
    Dim i As Long
    
    strDirectorys = Split(strDirectory, "\")
    If strDirectorys(0) = "" And strDirectorys(1) = "" Then
        strTemp = "\\" & strDirectorys(2)
        i = 3
    Else
        strTemp = strDirectorys(0)
        i = 1
    End If
    
    On Error GoTo PROC_ERR
    
    For i = i To UBound(strDirectorys)
        strTemp = strTemp & "\" & strDirectorys(i)
        If Dir(strTemp, vbDirectory) = "" Then
            Call MkDir(strTemp)
        End If
    Next i
    
PROC_ERR:
    Debug.Print "创建目录树的过程中发生错误：" & Err.Number & vbCrLf & Err.Description
    
End Function

' 以二进制方式读文件
' strFilename - 文件全路径
' lngOffset - 读取的偏移量，以1为基数
' lngLength - 读取的内容长度，值为-1时表示读到文件末尾
' 返回值 - 读到的内容字节数组
Public Function ReadBinaryFile(strFilename As String, Optional lngOffset As Long = 1, Optional ByVal lngLength As Long = -1) As Byte()

    Dim lngFileNo As Long
    Dim bytData() As Byte
    
    If strFilename = "" Then Exit Function
    If Dir(strFilename) = "" Then Exit Function
    
    On Error GoTo PROC_ERR
    
    ' 打开文件
    lngFileNo = FreeFile
    Open strFilename For Binary As lngFileNo
    
    ' 申请存储读到内容的缓冲区
    If lngLength = -1 Then
        lngLength = LOF(lngFileNo) - lngOffset + 1
    End If
    ReDim bytData(LOF(lngFileNo) - lngOffset) As Byte
    
    ' 将文件内容读入缓冲区
    Get #lngFileNo, lngOffset, bytData
    
    ' 关闭文件
    Close lngFileNo
    
PROC_EXIT:
    ReadBinaryFile = bytData
    Erase bytData
    Exit Function
    
PROC_ERR:
    Close
    Debug.Print "读取文件的过程中发生错误：" & Err.Number & vbCrLf & Err.Description
    'MsgBox "读取文件的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 以文本方式读文件
' strFilename - 文件全路径
' lngOffset - 读取的偏移量，以1为基数
' lngLength - 读取的内容长度，值为-1时表示读到文件末尾
' 返回值 - 读到的内容字节数组
Public Function ReadFile(strFilename As String, Optional lngOffset As Long = 1, Optional ByVal lngLength As Long = -1) As String

    ReadFile = ""
    
    If strFilename = "" Then Exit Function
    If Dir(strFilename) = "" Then Exit Function
    
    ReadFile = StrConv(ReadBinaryFile(strFilename, lngOffset, lngLength), vbUnicode)
    
End Function

' 以二进制方式写文件
' strFilename - 文件全路径
' bytData - 要写入的内容字节数组
' lngOffset - 写入的偏移量，以1为基数
' 返回值 - 写入是否成功
Public Function WriteBinaryFile(strFilename As String, bytData() As Byte, Optional lngOffset As Long = 1) As Boolean

    Dim lngFileNo As Long
    
    WriteBinaryFile = False
    
    If strFilename = "" Then Exit Function
    
    On Error GoTo PROC_ERR
    
    lngFileNo = FreeFile
    
    Open strFilename For Binary As lngFileNo
    Put #lngFileNo, lngOffset, bytData
    Close lngFileNo
    
    WriteBinaryFile = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    Close
    Debug.Print "写入文件的过程中发生错误：" & Err.Number & vbCrLf & Err.Description
    'MsgBox "写入文件的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 以文本方式写文件
' strFilename - 文件全路径
' strData - 要写入的内容字符串
' lngWriteMode - 写入方式（Appand/Overwrite）
' blnConfirmOwerwrite - 如果要写入的文件已经存在，且写入模式是Overwrite，是否提示覆盖
' 返回值 - 写入是否成功
Public Function WriteFile(strFilename As String, strData As String, Optional lngWriteMode As WriteMode = Overwrite, Optional blnConfirmOwerwrite As Boolean = True) As Boolean

    Dim lngFileNo As Long
    
    WriteFile = False
    
    If strFilename = "" Then Exit Function
    If lngWriteMode = Overwrite And Dir(strFilename) <> "" Then
        If blnConfirmOwerwrite Then
            If MsgBox("文件已经存在，是否覆盖？", vbQuestion + vbYesNo, "提示") = vbNo Then GoTo PROC_EXIT
        End If
        Kill strFilename
    End If
    
    On Error GoTo PROC_ERR
    
    lngFileNo = FreeFile
    
    If lngWriteMode = Appand Then
        Open strFilename For Append As lngFileNo
    Else
        Open strFilename For Output As lngFileNo
    End If
    
    Print #lngFileNo, strData
    Close lngFileNo
    
    WriteFile = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    Close
    Debug.Print "写入文件的过程中发生错误：" & Err.Number & vbCrLf & Err.Description
    'MsgBox "写入文件的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function
