Attribute VB_Name = "GUIDUtilities"
' GUID 处理模块

Option Explicit

Private Declare Function CoCreateGuid Lib "OLE32.DLL" (pGUID As GUID) As Long

Private Declare Function StringFromGUID2 Lib "OLE32.DLL" (pGUID As GUID, ByVal PointerToString As Long, ByVal MaxLength As Long) As Long

Private Type GUID
    Guid1 As Long
    Guid2 As Long
    Guid3 As Long
    Guid4(0 To 7) As Byte
End Type

' 生成 GUID 字符串
Public Function GenerateGUID() As String

    Dim udtGUID As GUID
    Dim strGUID As String
    Dim lngRetVal As Long
    
    lngRetVal = CoCreateGuid(udtGUID)
    If lngRetVal <> 0 Then
        strGUID = ""
    Else
        strGUID = String$(38, 0)
        StringFromGUID2 udtGUID, StrPtr(strGUID), 39
    End If
    
    GenerateGUID = strGUID
    
End Function


