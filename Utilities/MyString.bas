Attribute VB_Name = "StringUtilities"
' 字符串处理模块

Option Explicit

' 格式化金额字符串，包含货币符号和千分位逗号，不足一元的补上前缀 0
Public Function FormatMoney(ccyMoney As Currency) As String

    Dim strMoney As String
    
    strMoney = FormatCurrency(ccyMoney)
    
    'strMoney = Replace(strMoney, "￥.", "￥0.") 字符串中居然找不到 ￥ 字符，怪！
    
    If InStr(strMoney, ".") = 2 Then
        strMoney = "￥0" & Mid(strMoney, 2)
    End If
    
    FormatMoney = strMoney
    
End Function

' 格式化金额字符串，不足一元的补上前缀 0
Public Function ToMoney(varMoney As Variant) As String

    Dim strMoney As String
    
    strMoney = Val(varMoney)
    
    If Left(strMoney, 1) = "." Then
        strMoney = "0" & strMoney
    End If
    
    ToMoney = strMoney
    
End Function

' 规范化数字字符串，小于 1 的补上前缀 0
Public Function RegulateNumber(strNumber As String) As String

    If Left(strNumber, 1) = "." Then
        strNumber = "0" & strNumber
    End If
    
    RegulateNumber = strNumber

End Function

' 从 strString 里边 读取 strBefore, strAfter 中间包含的字符串
Public Function GetSubString(strString As String, ByVal strBefore As String, ByVal strAfter As String, Optional lngStartPos As Long = 1) As String

    Dim lngPos1 As Long, lngPos2 As Long
    
    If lngStartPos < 1 Then lngStartPos = 1
    lngPos1 = InStr(lngStartPos, strString, strBefore, vbTextCompare) + Len(strBefore)
    lngPos2 = InStr(lngPos1 + 1, strString, strAfter, vbTextCompare)
    
    If lngPos2 > lngPos1 And lngPos1 > Len(strBefore) Then
        GetSubString = Mid(strString, lngPos1, lngPos2 - lngPos1)
    Else
        GetSubString = ""
    End If
        
End Function

' 递增字符串类型的编号末尾的数字
Public Function IncreaseString(sIn As String, Optional iAmount As Long = 1) As String

    If iAmount < 1 Then
        iAmount = 1
    End If
    
    Dim iLength As Long
    
    iLength = Len(sIn)
    
    Dim sOut As String
    
    If IsNumeric(sIn) Then
    
        sOut = Format(Val(sIn) + iAmount, String(iLength, "0"))
        
    Else
    
        Dim iPresIngLength As Long
        Dim i As Long
        
        For i = iLength To 1 Step -1
            If Not IsNumeric(Mid(sIn, i, 1)) Then
                iPresIngLength = i
                Exit For
            End If
        Next i
        
        If iPresIngLength = iLength Then
            sOut = sIn
        Else
            sOut = Left(sIn, iPresIngLength) & Format(Val(Mid(sIn, iPresIngLength + 1)) + iAmount, String(iLength - iPresIngLength, "0"))
        End If
        
    End If
    
    IncreaseString = sOut
    
End Function

' 获取字符串的长度（字节数）
Public Function StrLen(sIn As String) As Long

    If IsNull(sIn) Then
        StrLen = 0
    Else
        StrLen = LenB(StrConv(sIn, vbFromUnicode))
    End If
    
End Function

' 获取在指定字节长度范围内的 Unicode 字符串的长度（字符数）
Public Function UStrLen(sIn As String, iBytesLimited As Long) As Long

    Dim i As Long
    Dim iBytes As Long
    
    UStrLen = Len(sIn)
    
    For i = 1 To UStrLen
    
        iBytes = iBytes + StrLen(Mid(sIn, i, 1))
        
        If iBytes > iBytesLimited Then
            UStrLen = i - 1
            Exit For
        End If
        
    Next i
    
End Function

