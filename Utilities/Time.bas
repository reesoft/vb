Attribute VB_Name = "TimeUtilities"
' 时间处理模块

Option Explicit

' 日期格式
Global Const gc_DateFormat = "yyyy-mm-dd"
Global Const gc_TimeFormat = "yyyy-mm-dd hh:MM:ss"

Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Declare Sub GetLocalTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

' 延时
Public Sub Delay(dblSeconds As Double)
    
    Dim timeTemp As SYSTEMTIME
    Dim dblStart As Double, dblEnd As Double
    
    Call GetLocalTime(timeTemp)
    
    With timeTemp
        dblStart = (((.wYear * 365# + .wMonth * 30# + .wDay) * 24# + .wHour) * 3600# + .wMinute * 60 + .wSecond) * 1000 + .wMilliseconds
    End With
    
    dblEnd = dblStart + dblSeconds * 1000
    
    Do While dblStart < dblEnd
        DoEvents
        DoEvents
        Call GetLocalTime(timeTemp)
        With timeTemp
            dblStart = (((.wYear * 365# + .wMonth * 30# + .wDay) * 24# + .wHour) * 3600# + .wMinute * 60 + .wSecond) * 1000 + .wMilliseconds
        End With
        DoEvents
        DoEvents
    Loop
    
End Sub

' 取得以毫秒计的当前时间戳记
Public Function GetMilliseconds() As Long

    Dim timeTemp As SYSTEMTIME
    
    Call GetLocalTime(timeTemp)
    
    With timeTemp
        GetMilliseconds = (.wHour * 3600# + .wMinute * 60 + .wSecond) * 1000 + .wMilliseconds
    End With
    
End Function

' 判断一个年份是否闰年
' By Larry Serflaten
Function IsLeapYearA(ByVal yr As Integer) As Boolean

    If ((yr Mod 4) = 0) Then
       IsLeapYearA = ((yr Mod 100) > 0) Or ((yr Mod 400) = 0)
    End If
    
End Function

' 判断一个年份是否闰年
' By Douglas Marquardt
Public Function IsLeapYearB(ByVal yr As Integer) As Boolean

    IsLeapYearB = Day(DateSerial(yr, 2, 29)) = 29
    
End Function

' 计算一个月份的天数
Function DaysInMonth(YearValue As Long, MonthValue As Long) As Long

    DaysInMonth = Day(DateSerial(YearValue, MonthValue + 1, 0))
    
End Function

' 判定是否一个月的最后一天
Private Function IsLastDayOfMonth(ByVal d As Date) As Boolean

    IsLastDayOfMonth = CBool(Month(d) - Month(DateAdd("d", 1, d)))
    
End Function

' 计算一个月的最后一天
Private Function LastDayOfMonth(ByVal srcDate As Date) As Date

    Dim d As Date
    d = DateAdd("m", 1, srcDate)
    LastDayOfMonth = DateAdd("d", -Day(d), d)
    
End Function
