Attribute VB_Name = "CurrencyUtilities"
' 格式化货币模块

Option Explicit

Global Const gc_CurrencySymbolDefault = "￥"

' 货币名称
Global g_CurrencyName As String

' 货币符号
Global g_CurrencySymbol As String

Private Declare Function GetCurrencyFormatBynum& Lib "kernel32" Alias "GetCurrencyFormatA" (ByVal Locale As Long, ByVal dwFlags As Long, ByVal lpValue As String, ByVal lpFormat As Long, ByVal lpCurrencyStr As String, ByVal cchCurrency As Long)

' 按指定的货币符号格式化货币
Public Function FormatCurrencyL(ccy As Currency, Optional Symbol As Variant) As String

    Dim strCurrency As String
    
    strCurrency = FormatCurrency(1)
    
    If InStr(strCurrency, "1") = 0 Then
        FormatCurrencyL = strCurrency
    Else
        strCurrency = Left(strCurrency, InStr(strCurrency, "1") - 1)
        If IsMissing(Symbol) Then
            FormatCurrencyL = Replace(FormatCurrency(ccy), strCurrency, g_CurrencySymbol, 1, 1)
        Else
            Symbol = CStr(Symbol)
            FormatCurrencyL = Replace(FormatCurrency(ccy), strCurrency, Symbol, 1, 1)
        End If
    End If
    
End Function

' 按中国地区设置格式化货币
Public Function FormatCurrencyCHS(ccy As Currency) As String

    Const c_Locale = 2052
    
    Dim strCurrency As String
    Dim lngLength As Long, lngReturn As Long
    
    strCurrency = Space(255)
    lngLength = 255
    
    lngReturn = GetCurrencyFormatBynum(c_Locale, 0, CStr(ccy), &O0, strCurrency, lngLength)
    
    If lngReturn = 0 Then
        FormatCurrencyCHS = ""
    Else
        strCurrency = Trim(strCurrency)
        If Asc(Right(strCurrency, 1)) = 0 Then
            FormatCurrencyCHS = Left(strCurrency, Len(strCurrency) - 1)
        Else
            FormatCurrencyCHS = strCurrency
        End If
    End If
    
End Function

' 恢复货币名称和符号为默认值
Public Sub RestoreCurrency()

    g_CurrencyName = ""
    g_CurrencySymbol = gc_CurrencySymbolDefault
    
End Sub
