VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBase64"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Encrypt"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
' BASE64 编码/解码/加密类模块

Option Explicit

' 编码/解码中间结构
Private Type MyByte
    B1 As Long
    B2 As Long
    B3 As Long
    B4 As Long
End Type

' 默认的编码字符串和尾部填充字符，修改它们时可实现简单加密
Private Const BASE64 As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
Private Const FILLCHAR As String = "="

' 编码字符串和填充字符串
Private m_strEncryptKey As String
Private m_strFillCharacter As String
' 编码字符串和填充字符串的副本，为加快运算速度定义为字节数组和整数类型
Private m_bytEncryptKey() As Byte
Private m_lngFillCharacter As Long

' 类初始化
Private Sub Class_Initialize()

    m_strEncryptKey = BASE64
    m_bytEncryptKey = CVar(StrConv(m_strEncryptKey, vbFromUnicode))
    m_strFillCharacter = FILLCHAR
    m_lngFillCharacter = Asc(m_strFillCharacter)
    
End Sub

' 设置填充字符
Public Property Let FillCharacter(ByVal vData As String)

    If Len(vData) <> 1 Then
        Exit Property
    End If
    
    ' 填充字符只能是可打印字符，且不能是加密键中的一个，否则设置填充字符失败
    If Asc(vData) > 31 And Asc(vData) < 127 Then
        If InStr(m_strEncryptKey, vData) <= 0 Then
            m_strFillCharacter = vData
            m_lngFillCharacter = Asc(m_strFillCharacter)
        End If
    End If
    
End Property

' 读取填充字符
Public Property Get FillCharacter() As String

    FillCharacter = m_strFillCharacter
    
End Property

' 设置编码字符串
Public Property Let EncryptKey(ByVal vData As String)

    Dim i As Long, j As Long
    Dim bytData() As Byte
    
    ' 编码字符串的长度必须是64字符
    If Len(vData) <> 64 Then
        Exit Property
    End If
    
    ' 编码字符串不能包含尾部填充字符
    If InStr(vData, m_strFillCharacter) > 0 Then
        Exit Property
    End If
    
    ' 编码字符串只能是可打印字符
    bytData = CVar(StrConv(vData, vbFromUnicode))
    For i = 0 To 63
        If bytData(i) > 31 And bytData(i) < 127 Then
            ' 编码字符串中不能有重复的字符
            For j = i + 1 To 63
                If bytData(i) = bytData(j) Then
                    Exit Property
                End If
            Next j
        Else
            Exit Property
        End If
    Next i
    
    m_strEncryptKey = vData
    m_bytEncryptKey = CVar(StrConv(m_strEncryptKey, vbFromUnicode))
    
End Property

' 读取编码字符串
Public Property Get EncryptKey() As String

    EncryptKey = m_strEncryptKey
    
End Property

' 对字符串进行解码，输出字符串
Public Function Decrypt(ByVal strSrc As String) As String

    Dim i As Long, lngLength As Long, lngPos As Long
    Dim bytSrc() As Byte
    Dim bytDecode() As Byte
    Dim mbtSrc As MyByte
    Dim strOut As String
    
    If strSrc = "" Then
        Decrypt = ""
        Exit Function
    End If
    
    ' 转换源字符串为字节数组
    bytSrc = CVar(StrConv(strSrc, vbFromUnicode))
    
    ' 计算字符数
    lngLength = UBound(bytSrc) + 1
    ' 字符数不能小于4，否则不能解码
    If lngLength < 4 Then
        Decrypt = ""
        Exit Function
    End If
    
    ' 分配输出缓冲区
    ReDim bytDecode(Int(lngLength / 4) * 3 - 1) As Byte
    
    ' 分块解码
    lngPos = 0
    i = 0
    Do While i < lngLength - 3
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = bytSrc(i + 1)
        mbtSrc.B3 = bytSrc(i + 2)
        mbtSrc.B4 = bytSrc(i + 3)
        i = i + 4
        Call FourToThree(mbtSrc)
        bytDecode(lngPos) = mbtSrc.B1
        bytDecode(lngPos + 1) = mbtSrc.B2
        bytDecode(lngPos + 2) = mbtSrc.B3
        lngPos = lngPos + 3
    Loop
    
    ' 由最后一块决定解码结果字符串的长度
    If mbtSrc.B4 > 0 And mbtSrc.B4 < 3 Then
        ReDim Preserve bytDecode(lngPos - 1 + mbtSrc.B4 - 3) As Byte
    End If
    
    Decrypt = StrConv(bytDecode, vbUnicode)
    
End Function

' 对字符串进行解码，输出字节流
Public Function DecryptToByte(ByVal strSrc As String) As Byte()

    Dim i As Long, lngLength As Long, lngPos As Long
    Dim bytSrc() As Byte
    Dim bytDecode() As Byte
    Dim mbtSrc As MyByte
    Dim strOut As String
    
    If strSrc = "" Then
        DecryptToByte = bytDecode
        Exit Function
    End If
    
    ' 转换源字符串为字节数组
    bytSrc = CVar(StrConv(strSrc, vbFromUnicode))
    
    ' 计算字符数
    lngLength = UBound(bytSrc) + 1
    ' 字符数不能小于4，否则不能解码
    If lngLength < 4 Then
        DecryptToByte = bytDecode
        Exit Function
    End If
    
    ' 分配输出缓冲区
    ReDim bytDecode(Int(lngLength / 4) * 3 - 1) As Byte
    
    ' 分块解码
    lngPos = 0
    i = 0
    Do While i < lngLength - 3
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = bytSrc(i + 1)
        mbtSrc.B3 = bytSrc(i + 2)
        mbtSrc.B4 = bytSrc(i + 3)
        i = i + 4
        Call FourToThree(mbtSrc)
        bytDecode(lngPos) = mbtSrc.B1
        bytDecode(lngPos + 1) = mbtSrc.B2
        bytDecode(lngPos + 2) = mbtSrc.B3
        lngPos = lngPos + 3
    Loop
    
    ' 由最后一块决定解码结果字符串的长度
    If mbtSrc.B4 > 0 And mbtSrc.B4 < 3 Then
        ReDim Preserve bytDecode(lngPos - 1 + mbtSrc.B4 - 3) As Byte
    End If
    
    DecryptToByte = bytDecode
    
End Function

' 对字符串进行编码，输出字符串
Public Function Encrypt(ByVal strSrc As String) As String

    Dim bytSrc() As Byte
    Dim bytEncode() As Byte
    Dim mbtSrc As MyByte
    Dim lngLength As Long
    Dim i As Long
    Dim lngPos As Long
    
    If strSrc = "" Then
        Encrypt = ""
        Exit Function
    End If
    
    ' 转换源字符串为字节数组
    bytSrc = CVar(StrConv(strSrc, vbFromUnicode))
    
    ' 计算字符数
    lngLength = UBound(bytSrc) + 1
    
    ' 分配输出缓冲区
    ReDim bytEncode(Int((lngLength + 2) / 3) * 4 - 1) As Byte
    
    ' 分块编码
    lngPos = 0
    For i = 0 To lngLength - 3 Step 3
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = bytSrc(i + 1)
        mbtSrc.B3 = bytSrc(i + 2)
        mbtSrc.B4 = 3 ' 块长度
        ' 编码
        Call ThreeToFour(mbtSrc)
        ' 保存结果
        bytEncode(lngPos) = mbtSrc.B1
        bytEncode(lngPos + 1) = mbtSrc.B2
        bytEncode(lngPos + 2) = mbtSrc.B3
        bytEncode(lngPos + 3) = mbtSrc.B4
        lngPos = lngPos + 4
    Next i
    
    ' 对最后一块编码
    If lngLength Mod 3 = 1 Then ' 最后一块长度为 1
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = 0
        mbtSrc.B3 = 0
        mbtSrc.B4 = 1 ' 块长度
        ' 编码
        Call ThreeToFour(mbtSrc)
        ' 保存结果
        bytEncode(lngPos) = mbtSrc.B1
        bytEncode(lngPos + 1) = mbtSrc.B2
        bytEncode(lngPos + 2) = mbtSrc.B3
        bytEncode(lngPos + 3) = mbtSrc.B4
    ElseIf lngLength Mod 3 = 2 Then ' 最后一块长度为 2
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = bytSrc(i + 1)
        mbtSrc.B3 = 0
        mbtSrc.B4 = 2 ' 块长度
        ' 编码
        Call ThreeToFour(mbtSrc)
        ' 保存结果
        bytEncode(lngPos) = mbtSrc.B1
        bytEncode(lngPos + 1) = mbtSrc.B2
        bytEncode(lngPos + 2) = mbtSrc.B3
        bytEncode(lngPos + 3) = mbtSrc.B4
    End If
    
    ' 输出结果
    Encrypt = StrConv(bytEncode, vbUnicode)
    
End Function

' 对字符串进行编码，输出字符串
Public Function EncryptFromByteArray(ByRef bytSrc() As Byte) As String

    Dim bytEncode() As Byte
    Dim mbtSrc As MyByte
    Dim lngLength As Long
    Dim i As Long
    Dim lngPos As Long
    
    If IsEmpty(bytSrc) Then
        EncryptFromByteArray = ""
        Exit Function
    End If
    
    ' 计算字符数
    lngLength = UBound(bytSrc) + 1
    
    ' 分配输出缓冲区
    ReDim bytEncode((lngLength + 2) / 3 * 4 - 1) As Byte
    
    ' 分块编码
    lngPos = 0
    For i = 0 To lngLength - 3 Step 3
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = bytSrc(i + 1)
        mbtSrc.B3 = bytSrc(i + 2)
        mbtSrc.B4 = 3 ' 块长度
        ' 编码
        Call ThreeToFour(mbtSrc)
        ' 保存结果
        bytEncode(lngPos) = mbtSrc.B1
        bytEncode(lngPos + 1) = mbtSrc.B2
        bytEncode(lngPos + 2) = mbtSrc.B3
        bytEncode(lngPos + 3) = mbtSrc.B4
        lngPos = lngPos + 4
    Next i
    
    ' 对最后一块编码
    If lngLength Mod 3 = 1 Then ' 最后一块长度为 1
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = 0
        mbtSrc.B3 = 0
        mbtSrc.B4 = 1 ' 块长度
        ' 编码
        Call ThreeToFour(mbtSrc)
        ' 保存结果
        bytEncode(lngPos) = mbtSrc.B1
        bytEncode(lngPos + 1) = mbtSrc.B2
        bytEncode(lngPos + 2) = mbtSrc.B3
        bytEncode(lngPos + 3) = mbtSrc.B4
    ElseIf lngLength Mod 3 = 2 Then ' 最后一块长度为 2
        mbtSrc.B1 = bytSrc(i)
        mbtSrc.B2 = bytSrc(i + 1)
        mbtSrc.B3 = 0
        mbtSrc.B4 = 2 ' 块长度
        ' 编码
        Call ThreeToFour(mbtSrc)
        ' 保存结果
        bytEncode(lngPos) = mbtSrc.B1
        bytEncode(lngPos + 1) = mbtSrc.B2
        bytEncode(lngPos + 2) = mbtSrc.B3
        bytEncode(lngPos + 3) = mbtSrc.B4
    End If
    
    ' 输出结果
    EncryptFromByteArray = StrConv(bytEncode, vbUnicode)
    
End Function

' 将 3 字节内容编码为 4 字节密文，mbtSrc.B4 存储要编码的内容长度
Private Sub ThreeToFour(ByRef mbtSrc As MyByte)

    Dim lngOut1 As Long, lngOut2 As Long, lngOut3 As Long, lngOut4 As Long
    Dim lngLength As Long, lngTotal As Long
    
    ' 编码的内容长度必须是 1 - 3 字节
    If mbtSrc.B4 <= 0 Or mbtSrc.B4 > 4 Or mbtSrc.B1 < 0 Or mbtSrc.B2 < 0 Or mbtSrc.B3 < 0 Or mbtSrc.B1 > 255 Or mbtSrc.B2 > 255 Or mbtSrc.B3 > 255 Then
        mbtSrc.B1 = m_lngFillCharacter
        mbtSrc.B2 = m_lngFillCharacter
        mbtSrc.B3 = m_lngFillCharacter
        mbtSrc.B4 = m_lngFillCharacter
        Exit Sub
    End If
    
    lngLength = mbtSrc.B4
    
    ' 合并 3 字节内容，拆分为 4 个 0 - 63 之间的整数
    lngTotal = mbtSrc.B1 * 256 * 256 + mbtSrc.B2 * 256 + mbtSrc.B3
    lngOut1 = (Int(lngTotal / 2 ^ 18) And 63)
    lngOut2 = (Int(lngTotal / 2 ^ 12) And 63)
    lngOut3 = (Int(lngTotal / 2 ^ 6) And 63)
    lngOut4 = (lngTotal And 63)
    
    ' 计算结果
    Select Case lngLength
        Case 1
            mbtSrc.B1 = m_bytEncryptKey(lngOut1)
            mbtSrc.B2 = m_bytEncryptKey(lngOut2)
            mbtSrc.B3 = m_lngFillCharacter
            mbtSrc.B4 = m_lngFillCharacter
        Case 2
            mbtSrc.B1 = m_bytEncryptKey(lngOut1)
            mbtSrc.B2 = m_bytEncryptKey(lngOut2)
            mbtSrc.B3 = m_bytEncryptKey(lngOut3)
            mbtSrc.B4 = m_lngFillCharacter
        Case 3
            mbtSrc.B1 = m_bytEncryptKey(lngOut1)
            mbtSrc.B2 = m_bytEncryptKey(lngOut2)
            mbtSrc.B3 = m_bytEncryptKey(lngOut3)
            mbtSrc.B4 = m_bytEncryptKey(lngOut4)
        Case Else
            mbtSrc.B1 = m_lngFillCharacter
            mbtSrc.B2 = m_lngFillCharacter
            mbtSrc.B3 = m_lngFillCharacter
            mbtSrc.B4 = m_lngFillCharacter
    End Select
    
End Sub

' 将 4 字节密文解码为 3 字节内容，解码后 mbtSrc.B1 - B3 存储解码内容，mbtSrc.B4 存储解码内容的长度
Private Sub FourToThree(ByRef mbtSrc As MyByte)

    Dim lngIn1 As Long, lngIn2 As Long, lngIn3 As Long, lngIn4 As Long
    Dim lngLength As Long, lngTotal As Long
    
    ' 检验输入内容的有效性
    If mbtSrc.B1 < 32 Or mbtSrc.B2 < 32 Or mbtSrc.B3 < 32 Or mbtSrc.B4 < 32 Or mbtSrc.B1 > 126 Or mbtSrc.B2 > 126 Or mbtSrc.B3 > 126 Or mbtSrc.B4 > 126 Then
        mbtSrc.B1 = 0
        mbtSrc.B2 = 0
        mbtSrc.B3 = 0
        mbtSrc.B4 = 0
        Exit Sub
    End If
    
    ' 将编码转换为位置序号
    lngIn1 = InStr(m_strEncryptKey, Chr(mbtSrc.B1)) - 1
    lngIn2 = InStr(m_strEncryptKey, Chr(mbtSrc.B2)) - 1
    lngIn3 = InStr(m_strEncryptKey, Chr(mbtSrc.B3)) - 1
    lngIn4 = InStr(m_strEncryptKey, Chr(mbtSrc.B4)) - 1
    If lngIn1 < 0 Or lngIn2 < 0 Then
        ' 异常情况，解码失败
        lngLength = 0
        lngIn1 = 0
        lngIn2 = 0
        lngIn3 = 0
        lngIn4 = 0
    ElseIf lngIn3 < 0 Then
        ' 原文仅有 1 个字符
        lngLength = 1
        lngIn3 = 0
        lngIn4 = 0
    ElseIf lngIn4 < 0 Then
        ' 原文有 2 个字符
        lngLength = 2
        lngIn4 = 0
    Else
        ' 原文是 3 个字符
        lngLength = 3
    End If
    
    ' 将位置序号合并为原文输出
    lngTotal = lngIn1 * 64 * 64 * 64 + lngIn2 * 64 * 64 + lngIn3 * 64 + lngIn4
    mbtSrc.B1 = Int(lngTotal / 2 ^ 16) And 255
    mbtSrc.B2 = Int(lngTotal / 2 ^ 8) And 255
    mbtSrc.B3 = lngTotal And 255
    mbtSrc.B4 = lngLength
    
End Sub
