Attribute VB_Name = "SkinUtilities"
Option Explicit

Public Declare Function LoadSkin Lib "Skin.dll" (ByVal sFilename As String) As Long
Public Declare Sub UnloadSkin Lib "Skin.dll" ()

Public Const CommonBackColor As Long = &HFFE6CC

' 统一设置窗口控件的背景颜色
Public Sub SetSkin(frm As Form)

    On Error Resume Next
    
    frm.BackColor = CommonBackColor
    
    Dim obj As Control
    
    For Each obj In frm.Controls
    
        Select Case TypeName(obj)
            Case "Label"
                obj.BackStyle = 0
            Case "CommandButton"
                obj.Style = 1
                obj.BackColor = CommonBackColor
            Case "OptionButton", "CheckBox", "Frame", "SSTab", "lc"
                obj.BackColor = CommonBackColor
            Case Else
        End Select
        
    Next
    
End Sub

