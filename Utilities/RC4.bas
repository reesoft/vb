Attribute VB_Name = "RC4EncryptUtilities"
' Copyright 2002 Richard, reesoft@163.com. All Rights Reserved.

' RC4加/解密模块(v1.2)
' 功能描述：本模块提供四个函数实现不同的功能
'     RC4EncryptionBS     - 输入 Byte 数组，输出 Unicode 字符串
'     RC4EncryptionByte   - 输入 Byte 数组，输出 Byte 数组
'     RC4EncryptionSS     - 输入 Unicode 字符串，输出 Unicode 字符串
'     RC4EncryptionString - 输入 Unicode 字符串，输出 Byte 数组
'     在调用以上函数前，须调用 InitRC4Encryption 过程初始化加密种子数组

Option Explicit

' 加密种子数组
Dim m_Seed(0 To 255) As Long

' 根据密码（种子）字符串初始化加密种子数组
' 数组有 256 个元素，各元素取值从 0 到 255，无重复值
' strPassword 为输入的密码（种子）
Private Sub InitRC4Encryption(strPassword As String)

    Dim i As Long, j As Long, lngA As Long
    Dim aSeed(0 To 255) As Byte
    
    If strPassword = "" Then strPassword = "*"
    
    ' 将 Unicode 字符串处理成 ANSI 字符串，使密码可以接受汉字
    strPassword = StrConv(strPassword, vbFromUnicode)
    
    ' 生成密码数组序列。例如密码为 "12345"，数组序列为 "1234512345..."
    lngA = LenB(strPassword)
    j = 0
    For i = 0 To 255
        j = j + 1
        If j > lngA Then j = 1
        aSeed(i) = AscB(MidB(strPassword, j, 1))
    Next i
    
    ' 种子数组序列各元素的初始值为 0 到 255 依次递增
    For i = 0 To 255
        m_Seed(i) = i
    Next i
    
    ' 根据密码数组序列重新排列种子数组序列的元素
    j = 0
    For i = 0 To 255
        j = (j + m_Seed(i) + aSeed(i)) Mod 256
        ' 交换 m_Seed(i) 和 m_Seed(j) 的值
        lngA = m_Seed(i)
        m_Seed(i) = m_Seed(j)
        m_Seed(j) = lngA
    Next i
    
End Sub

' RC4 加密/解密函数
' 输入 Byte 数组，输出 Unicode 字符串
Public Function RC4EncryptionBS(strPassword As String, bytIn() As Byte) As String

    RC4EncryptionBS = StrConv(RC4EncryptionByte(strPassword, bytIn), vbUnicode)
    
End Function

' RC4 加密/解密函数
' 输入 Byte 数组，输出 Byte 数组
Public Function RC4EncryptionByte(strPassword As String, bytIn() As Byte) As Byte()

    Dim lngLength As Long, lngA As Long, i As Long, j As Long, k As Long
    Dim aOut() As Byte
    
    lngLength = UBound(bytIn) + 1
    
    If lngLength > 0 Then
        ReDim aOut(lngLength - 1) As Byte
        Call InitRC4Encryption(strPassword)
        j = 0
        k = 0
        For i = 1 To lngLength
            j = (j + 1) Mod 256
            k = (k + m_Seed(j)) Mod 256
            ' 交换 m_Seed(j) 和 m_Seed(k) 的值
            lngA = m_Seed(j)
            m_Seed(j) = m_Seed(k)
            m_Seed(k) = lngA
            
            ' 原文与加密种子数组的元素异或，生成密文
            aOut(i - 1) = bytIn(i - 1) Xor m_Seed((m_Seed(j) + m_Seed(k)) Mod 256)
        Next i
    End If
    
    RC4EncryptionByte = aOut
    
End Function

' RC4 加密/解密
' 输入 Unicode 字符串，输出 Unicode 字符串
Public Function RC4EncryptionSS(strPassword As String, strIn As String) As String

    RC4EncryptionSS = StrConv(RC4EncryptionString(strPassword, strIn), vbUnicode)
    
End Function

' RC4 加密/解密
' 输入 Unicode 字符串，输出 Byte 数组
Public Function RC4EncryptionString(strPassword As String, strIn As String) As Byte()

    Dim lngLength As Long, lngA As Long, i As Long, j As Long, k As Long
    Dim aOut() As Byte
    
    strIn = StrConv(strIn, vbFromUnicode)
    lngLength = LenB(strIn)
    
    If lngLength > 0 Then
        ReDim aOut(lngLength - 1) As Byte
        Call InitRC4Encryption(strPassword)
        j = 0
        k = 0
        For i = 1 To lngLength
            j = (j + 1) Mod 256
            k = (k + m_Seed(j)) Mod 256
            ' 交换 m_Seed(j) 和 m_Seed(k) 的值
            lngA = m_Seed(j)
            m_Seed(j) = m_Seed(k)
            m_Seed(k) = lngA
            
            ' 原文与加密种子数组的元素异或，生成密文
            aOut(i - 1) = AscB(MidB(strIn, i, 1)) Xor m_Seed((m_Seed(j) + m_Seed(k)) Mod 256)
        Next i
    End If
    
    RC4EncryptionString = aOut
    
End Function
