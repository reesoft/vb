Attribute VB_Name = "ChineseUtilities"
' 中文数字处理模块

Option Explicit

Global CDigit(7) As String
Global MoneyDigit(13) As String
Global CNumber(31) As String
Global RNumber(10) As String

Dim m_blnInitFlag As Boolean

' 初始化变量
Public Sub InitCNumber()

    CDigit(0) = "零"
    CDigit(1) = "拾"
    CDigit(2) = "佰"
    CDigit(3) = "仟"
    CDigit(4) = "万"
    CDigit(5) = "亿"
    CDigit(6) = "兆"
    
    MoneyDigit(0) = "分"
    MoneyDigit(1) = "角"
    MoneyDigit(2) = "元"
    MoneyDigit(3) = "拾"
    MoneyDigit(4) = "佰"
    MoneyDigit(5) = "仟"
    MoneyDigit(6) = "万"
    MoneyDigit(7) = "拾"
    MoneyDigit(8) = "佰"
    MoneyDigit(9) = "仟"
    MoneyDigit(10) = "亿"
    MoneyDigit(11) = "拾"
    MoneyDigit(12) = "佰"
    MoneyDigit(13) = "仟"
    
    CNumber(0) = "零"
    CNumber(1) = "壹"
    CNumber(2) = "贰"
    CNumber(3) = "叁"
    CNumber(4) = "肆"
    CNumber(5) = "伍"
    CNumber(6) = "陆"
    CNumber(7) = "柒"
    CNumber(8) = "捌"
    CNumber(9) = "玖"
    CNumber(10) = "拾"
    CNumber(11) = "拾壹"
    CNumber(12) = "拾贰"
    CNumber(13) = "拾叁"
    CNumber(14) = "拾肆"
    CNumber(15) = "拾伍"
    CNumber(16) = "拾陆"
    CNumber(17) = "拾柒"
    CNumber(18) = "拾捌"
    CNumber(19) = "拾玖"
    CNumber(20) = "贰拾"
    CNumber(21) = "贰拾壹"
    CNumber(22) = "贰拾贰"
    CNumber(23) = "贰拾叁"
    CNumber(24) = "贰拾肆"
    CNumber(25) = "贰拾伍"
    CNumber(26) = "贰拾陆"
    CNumber(27) = "贰拾柒"
    CNumber(28) = "贰拾捌"
    CNumber(29) = "贰拾玖"
    CNumber(30) = "叁拾零"
    CNumber(31) = "叁拾壹"
    
    RNumber(0) = "○"
    RNumber(1) = "Ⅰ"
    RNumber(2) = "Ⅱ"
    RNumber(3) = "Ⅲ"
    RNumber(4) = "Ⅳ"
    RNumber(5) = "Ⅴ"
    RNumber(6) = "Ⅵ"
    RNumber(7) = "Ⅶ"
    RNumber(8) = "Ⅷ"
    RNumber(9) = "Ⅸ"
    RNumber(10) = "Ⅹ"
    
    m_blnInitFlag = True
    
End Sub

' 把数字转换为中文，忽略小数部分
Public Function ChineseNumber(ByVal dblIn As Double) As String

    On Error Resume Next
    
    If Not m_blnInitFlag Then Call InitCNumber
    
    If dblIn < 0 Then
        ChineseNumber = "负" & ChineseNumber(-dblIn)
        Exit Function
    End If
    
    If dblIn < 10 Then
        ChineseNumber = CNumber(dblIn)
    ElseIf dblIn = 10 Then
        ChineseNumber = CNumber(1) & CNumber(10)
    ElseIf dblIn < 20 Then
        ChineseNumber = CNumber(1) & CNumber(10) & CNumber(dblIn - 10)
    ElseIf dblIn < 100 Then
        If dblIn Mod 10 > 0 Then
            ChineseNumber = CNumber(Fix(dblIn / 10)) & CDigit(1) & CNumber(dblIn Mod 10)
        Else
            ChineseNumber = CNumber(Fix(dblIn / 10)) & CDigit(1)
        End If
    ElseIf dblIn < 1000 Then
        If dblIn Mod 100 = 0 Then
            ChineseNumber = CNumber(Fix(dblIn / 100)) & CDigit(2)
        ElseIf dblIn Mod 100 < 10 Then
            ChineseNumber = CNumber(Fix(dblIn / 100)) & CDigit(2) & CNumber(0) & CNumber(dblIn Mod 100)
        ElseIf dblIn Mod 100 < 20 Then
            ChineseNumber = CNumber(Fix(dblIn / 100)) & CDigit(2) & CNumber(1) & CNumber(dblIn Mod 100)
        Else
            ChineseNumber = CNumber(Fix(dblIn / 100)) & CDigit(2) & ChineseNumber(dblIn Mod 100)
        End If
    ElseIf dblIn < 10000 Then
        If dblIn Mod 1000 = 0 Then
            ChineseNumber = CNumber(Fix(dblIn / 1000)) & CDigit(3)
        ElseIf dblIn Mod 1000 < 100 Then
            ChineseNumber = CNumber(Fix(dblIn / 1000)) & CDigit(3) & CNumber(0) & ChineseNumber(dblIn Mod 1000)
        Else
            ChineseNumber = CNumber(Fix(dblIn / 1000)) & CDigit(3) & ChineseNumber(dblIn Mod 1000)
        End If
    ElseIf dblIn < 100000000 Then
        If dblIn Mod 10000 = 0 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 10000)) & CDigit(4)
        ElseIf dblIn Mod 10000 < 1000 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 10000)) & CDigit(4) & CNumber(0) & ChineseNumber(dblIn Mod 10000)
        ElseIf Fix(dblIn / 10000) Mod 10 = 0 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 10000)) & CDigit(4) & CNumber(0) & ChineseNumber(dblIn Mod 10000)
        Else
            ChineseNumber = ChineseNumber(Fix(dblIn / 10000)) & CDigit(4) & ChineseNumber(dblIn Mod 10000)
        End If
    ElseIf dblIn < 1000000000000# Then
        If dblIn Mod 100000000 = 0 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 100000000)) & CDigit(5)
        ElseIf dblIn Mod 100000000 < 10000000 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 100000000)) & CDigit(5) & CNumber(0) & ChineseNumber(dblIn Mod 100000000)
        ElseIf Fix(dblIn / 100000000) Mod 10 = 0 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 100000000)) & CDigit(5) & CNumber(0) & ChineseNumber(dblIn Mod 100000000)
        Else
            ChineseNumber = ChineseNumber(Fix(dblIn / 100000000)) & CDigit(5) & ChineseNumber(dblIn Mod 100000000)
        End If
    Else
        If dblIn Mod 1000000000000# = 0 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 1000000000000#)) & CDigit(6)
        ElseIf dblIn Mod 1000000000000# < 10000000 Then
            ChineseNumber = ChineseNumber(Fix(dblIn / 1000000000000#)) & CDigit(6) & CNumber(0) & ChineseNumber(dblIn Mod 1000000000000#)
        Else
            ChineseNumber = ChineseNumber(Fix(dblIn / 1000000000000#)) & CDigit(6) & ChineseNumber(dblIn Mod 1000000000000#)
        End If
    End If
    
End Function

' 把数字转换为中文年份
Public Function ChineseYear(varYear As Variant) As String

    Dim intYear As Integer
    Dim strYear As String
    
    If Not m_blnInitFlag Then Call InitCNumber
    
    If Val(varYear) < 0 Or Val(varYear) > 3000 Then
        MsgBox "年份无效！", vbExclamation, "提示"
        strYear = "    "
        Exit Function
    End If
    
    intYear = Val(varYear)
    
    If varYear = "" Then
        strYear = "    "
'    ElseIf intYear < 10 Then
'        strYear = "贰零零" & CNumber(intYear)
    ElseIf intYear < 100 Then
        strYear = "贰零" & CNumber(Fix(intYear / 10)) & CNumber(intYear Mod 10)
    Else
        Do While intYear > 0
            strYear = CNumber(intYear Mod 10) & strYear
            intYear = Fix(intYear / 10)
        Loop
    End If
    
    ChineseYear = strYear
    
End Function

' 把数字转换为中文月份
Public Function ChineseMonth(varMonth As Variant) As String

    Dim intMonth As Integer
    Dim strMonth As String
    
    If Not m_blnInitFlag Then Call InitCNumber
    
    If Val(varMonth) < 0 Or Val(varMonth) > 12 Then
        MsgBox "月份无效！", vbExclamation, "提示"
        strMonth = "    "
        Exit Function
    End If
    
    intMonth = Val(varMonth)
    
    If intMonth = 0 Then
        strMonth = "    "
    ElseIf intMonth < 10 Then
        strMonth = "零" & CNumber(intMonth)
    ElseIf intMonth = 10 Then
        strMonth = "零壹拾"
    Else
        strMonth = "壹拾" & CNumber(intMonth Mod 10)
    End If
    
    ChineseMonth = strMonth
    
End Function

' 把数字转换为中文日期
Public Function ChineseDay(varDay As Variant) As String

    Dim intDay As Integer
    Dim strDay As String
    
    If Not m_blnInitFlag Then Call InitCNumber
    
    If Val(varDay) < 0 Or Val(varDay) > 31 Then
        MsgBox "日期无效！", vbExclamation, "提示"
        strDay = "    "
        Exit Function
    End If
    
    intDay = Val(varDay)
    
    If intDay = 0 Then
        strDay = "    "
    ElseIf intDay < 10 Then
        strDay = "零" & CNumber(intDay)
    ElseIf intDay = 10 Then
        strDay = "零壹拾"
    ElseIf intDay < 20 Then
        strDay = "壹" & CNumber(intDay)
    ElseIf intDay = 20 Then
        strDay = "零贰拾"
    ElseIf intDay = 30 Then
        strDay = "零叁拾"
    Else
        strDay = CNumber(intDay)
    End If
    
    ChineseDay = strDay
    
End Function

' 把金额转为中文
Public Function ChineseMoney(ByVal dblMoney As Double) As String

    Dim lngMoney As Long
    Dim strMoney As String, strPrefix As String
    
    If Not m_blnInitFlag Then Call InitCNumber
    
    If dblMoney < 0.01 Then
        ChineseMoney = ""
        Exit Function
    End If
    
    If dblMoney > 2147483647 Then
        strPrefix = ChineseNumber(Int(dblMoney / 100000000)) & "亿"
        dblMoney = dblMoney - Int(dblMoney / 100000000) * 100000000
    End If
    
    ' 加上货币名称（例如：人民币、美元……）
    strPrefix = g_CurrencyName & strPrefix
    
    lngMoney = Int(dblMoney)
    strMoney = Format(dblMoney * 100, "00")
    
    If Len(strMoney) >= 2 Then
        lngMoney = Val(Left(strMoney, Len(strMoney) - 2))
        strMoney = Right(strMoney, 2)
    Else
        lngMoney = 0
    End If
    
    If lngMoney > 0 Then
        If Val(Left(strMoney, 1)) > 0 Then
            If Val(Right(strMoney, 1)) > 0 Then
                If lngMoney / 10 = Int(lngMoney / 10) Then
                    ChineseMoney = strPrefix & ChineseNumber(lngMoney) & "元零" & ChineseNumber(Val(Left(strMoney, 1))) & "角" & ChineseNumber(Val(Right(strMoney, 1))) & "分"
                Else
                    ChineseMoney = strPrefix & ChineseNumber(lngMoney) & "元" & ChineseNumber(Val(Left(strMoney, 1))) & "角" & ChineseNumber(Val(Right(strMoney, 1))) & "分"
                End If
            Else
                If lngMoney / 10 = Int(lngMoney / 10) Then
                    ChineseMoney = strPrefix & ChineseNumber(lngMoney) & "元零" & ChineseNumber(Val(Left(strMoney, 1))) & "角整"
                Else
                    ChineseMoney = strPrefix & ChineseNumber(lngMoney) & "元" & ChineseNumber(Val(Left(strMoney, 1))) & "角整"
                End If
            End If
        ElseIf Val(Right(strMoney, 1)) > 0 Then
            ChineseMoney = strPrefix & ChineseNumber(lngMoney) & "元零" & ChineseNumber(Val(Right(strMoney, 1))) & "分"
        Else
            ChineseMoney = strPrefix & ChineseNumber(lngMoney) & "元整"
        End If
    Else
        If Val(Right(strMoney, 1)) > 0 Then
            ChineseMoney = strPrefix & ChineseNumber(Val(Left(strMoney, 1))) & "角" & ChineseNumber(Val(Right(strMoney, 1))) & "分"
        Else
            ChineseMoney = strPrefix & ChineseNumber(Val(Left(strMoney, 1))) & "角整"
        End If
    End If
    
End Function

' 把金额转为指定位数的中文
Public Function ChineseMoneyDigit(ByVal dblMoney As Double, ByVal lngDigit As Long) As String

    Dim strMoney As String, strOut As String
    Dim i As Long
    
    If Not m_blnInitFlag Then Call InitCNumber
    
    strMoney = Format(dblMoney * 100, "0")
    strMoney = Right(String(lngDigit, "0") & strMoney, lngDigit)
    
    For i = 1 To lngDigit
        If Mid(strMoney, i, 1) = "0" Then
            If Mid(strMoney, i + 1, 1) = "0" Then
                strOut = strOut & "" & MoneyDigit(lngDigit - i)
            Else
                strOut = strOut & "￥" & MoneyDigit(lngDigit - i)
            End If
        Else
            Exit For
        End If
    Next i
    
    Do While i <= lngDigit
        strOut = strOut & CNumber(Val(Mid(strMoney, i, 1))) & MoneyDigit(lngDigit - i)
        i = i + 1
    Loop
    
    ChineseMoneyDigit = strOut
    
End Function

