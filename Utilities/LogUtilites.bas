Attribute VB_Name = "LogUtilites"
' 写日志模块

Option Explicit

Public Enum LogLevel
    DebugLevel = 0
    InfoLevel = 1
    ErrorLevel = 2
End Enum

Private m_lngLogLevel As LogLevel
Private m_strLogFilePath As String

' 设置记录的模块级别，低于该级别的日志不记录到文件
Public Sub SetLogLevel(lngLevel As LogLevel)

    m_lngLogLevel = lngLevel
    
End Sub

' 设置日志文件路径
Public Sub SetLogFilePath(strLogFilePath As String)

    m_strLogFilePath = strLogFilePath
    
End Sub

' 转换日志级别为文本
Public Function GetLogLevelDesc(lngLevel As LogLevel) As String

    If lngLevel = DebugLevel Then
        GetLogLevelDesc = "DEBUG"
    ElseIf lngLevel = InfoLevel Then
        GetLogLevelDesc = "INFO"
    ElseIf lngLevel = ErrorLevel Then
        GetLogLevelDesc = "ERROR"
    End If
    
End Function

' 记录日志
Public Sub Log(lngLevel As LogLevel, strContent As String)

    If lngLevel < m_lngLogLevel Then
        Exit Sub
    End If
    
    Dim strLogLine As String
    
    strLogLine = "[" + Format(Now, "yyyy-mm-dd HH:MM:SS") + "][" + GetLogLevelDesc(lngLevel) + "][" + strContent + "]"

    If m_strLogFilePath <> "" Then
        FileUtilities.WriteFile m_strLogFilePath, strLogLine, Appand
    End If
    
End Sub

' 记录调试级别的日志
Public Sub LogDebug(strContent As String)

    Call Log(DebugLevel, strContent)
    
End Sub

' 记录信息级别的日志
Public Sub LogInfo(strContent As String)

    Call Log(InfoLevel, strContent)
        
End Sub

' 记录错误级别的日志
Public Sub LogError(strContent As String)

    Call Log(ErrorLevel, strContent)
    
End Sub

