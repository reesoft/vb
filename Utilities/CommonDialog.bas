Attribute VB_Name = "CommonDialogUtilities"
' 公用对话框模块

Option Explicit

Private Type OPENFILENAME
    lStructSize As Long
    hwndOwner As Long
    hInstance As Long
    lpstrFilter As String
    lpstrCustomFilter As String
    nMaxCustFilter As Long
    nFilterIndex As Long
    lpstrFile As String
    nMaxFile As Long
    lpstrFileTitle As String
    nMaxFileTitle As Long
    lpstrInitialDir As String
    lpstrTitle As String
    flags As Long
    nFileOffset As Integer
    nFileExtension As Integer
    lpstrDefExt As String
    lCustData As Long
    lpfnHook As Long
    lpTemplateName As String
End Type

Private Type BROWSEINFO
    hOwner As Long
    pidlRoot As Long
    pszDisplayName As String
    lpszTitle As String
    ulFlags As Long
    lpfn As Long
    lParam As Long
    iImage As Long
End Type

Const BIF_RETURNONLYFSDIRS = &H1 ' 仅允许浏览文件系统文件夹
Const BIF_DONTGOBELOWDOMAIN = &H2 ' 强制用户停留在网络邻居的域级别中
Const BIF_STATUSTEXT = &H4 ' 在选择对话框中显示状态栏
Const BIF_RETURNFSANCESTORS = &H8 ' 返回文件系统祖先
Const BIF_BRWOSEFORCOMPUTER = &H1000 ' 允许浏览计算机
Const BIF_BROWSEFORPRINTER = &H2000 ' 允许浏览打印机文件夹

Const CC_RGBINIT = &H1
Const CC_FULLOPEN = &H2
Const CC_PREVENTFULLOPEN = &H4
Const CC_SHOWHELP = &H8
Const CC_ENABLEHOOK = &H10
Const CC_ENABLETEMPLATE = &H20
Const CC_ENABLETEMPLATEHANDLE = &H40
Const CC_SOLIDCOLOR = &H80
Const CC_ANYCOLOR = &H100

Private Type udtCHOOSECOLOR
    lStructSize As Long
    hwndOwner As Long
    hInstance As Long
    rgbResult As Long
    lpCustColors As String
    flags As Long
    lCustData As Long
    lpfnHook As Long
    lpTemplateName As String
End Type

Const CF_SCREENFONTS = &H1
Const CF_PRINTERFONTS = &H2
Const CF_BOTH = (CF_SCREENFONTS Or CF_PRINTERFONTS)
Const CF_SHOWHELP = &H4&
Const CF_ENABLEHOOK = &H8&
Const CF_ENABLETEMPLATE = &H10&
Const CF_ENABLETEMPLATEHANDLE = &H20&
Const CF_INITTOLOGFONTSTRUCT = &H40&
Const CF_USESTYLE = &H80&
Const CF_EFFECTS = &H100&
Const CF_APPLY = &H200&
Const CF_ANSIONLY = &H400&
Const CF_SCRIPTSONLY = CF_ANSIONLY
Const CF_NOVECTORFONTS = &H800&
Const CF_NOOEMFONTS = CF_NOVECTORFONTS
Const CF_NOSIMULATIONS = &H1000&
Const CF_LIMITSIZE = &H2000&
Const CF_FIXEDPITCHONLY = &H4000&
Const CF_WYSIWYG = &H8000 '  must also have CF_SCREENFONTS CF_PRINTERFONTS
Const CF_FORCEFONTEXIST = &H10000
Const CF_SCALABLEONLY = &H20000
Const CF_TTONLY = &H40000
Const CF_NOFACESEL = &H80000
Const CF_NOSTYLESEL = &H100000
Const CF_NOSIZESEL = &H200000
Const CF_SELECTSCRIPT = &H400000
Const CF_NOSCRIPTSEL = &H800000
Const CF_NOVERTFONTS = &H1000000
Const BOLD_FONTTYPE = &H100
Const ITALIC_FONTTYPE = &H200
Const REGULAR_FONTTYPE = &H400
Const SCREEN_FONTTYPE = &H2000
Const PRINTER_FONTTYPE = &H4000
Const SIMULATED_FONTTYPE = &H8000
Const LF_FACESIZE = 32
'Const WM_CHOOSEFONT_GETLOGFONT = (WM_USER + 1)
'Const WM_CHOOSEFONT_SETLOGFONT = (WM_USER + 101)
'Const WM_CHOOSEFONT_SETFLAGS = (WM_USER + 102)

Private Type udtCHOOSEFONT
    lStructSize As Long
    hwndOwner As Long          '  caller's window handle
    hdc As Long                '  printer DC/IC or NULL
    lpLogFont As Long
    iPointSize As Long         '  10 * size in points of selected font
    flags As Long              '  enum. type flags
    rgbColors As Long          '  returned text color
    lCustData As Long          '  data passed to hook fn.
    lpfnHook As Long           '  ptr. to hook function
    lpTemplateName As String   '  custom template name
    hInstance As Long          '  instance handle of.EXE that contains cust. dlg. template
    lpszStyle As String        '  return the style field here, must be LF_FACESIZE or bigger
    nFontType As Integer       '  same value reported to the EnumFonts call back with the extra FONTTYPE_ bits added
    MISSING_ALIGNMENT As Integer
    nSizeMin As Long           '  minimum pt size allowed &
    nSizeMax As Long           '  max pt size allowed if CF_LIMITSIZE is used
End Type

Private Type udtLOGFONT
    lfHeight As Long            ' 按逻辑单位表示的字体高度
    lfWidth As Long             ' 按逻辑单位表示的字体宽度
    lfEscapement As Long        ' 文本的旋转角度，以 0.1 度为单位
    lfOrientation As Long       ' 设定值与 lfEscapement 相同（旋转方向？）
    lfWeight As Long            ' 字体的粗细，值在 100～900 之间
    lfItalic As Byte            ' 是否斜体
    lfUnderline As Byte         ' 是否带下划线
    lfStrikeOut As Byte         ' 是否带删除线
    lfCharSet As Byte           ' 文本所属的字符集
    lfOutPrecision As Byte      ' 图形设备接口，用来确定字体的精度
    lfClipPrecision As Byte     ' 文本超宽时剪裁区的精度
    lfQuality As Byte           ' 文本显示品质，可设定为 DEFAULT_QUALITY、DRAFT_QUALITY 或 PROOF_QUALITY
    lfPitchAndFamily As Byte    ' 字体的间距和家族。间距可为 DEFAULT_PITCH、FIXED_PITCH 或 VARIABLE_PITCH，
                                ' 且在两个低位的字节中设定。字体家族存放在 4～7 位，且可为 FF_DECORATIVE、
                                ' FF_DONTCARE、FF_MODERM、FF_ROMAN、FF_SCRIPT 或 FF_SWISS
    lfFaceName As String * LF_FACESIZE ' 包含字样名的缓冲区
End Type

Const PD_ALLPAGES = &H0
Const PD_SELECTION = &H1
Const PD_PAGENUMS = &H2
Const PD_NOSELECTION = &H4
Const PD_NOPAGENUMS = &H8
Const PD_COLLATE = &H10
Const PD_PRINTTOFILE = &H20
Const PD_PRINTSETUP = &H40
Const PD_NOWARNING = &H80
Const PD_RETURNDC = &H100
Const PD_RETURNIC = &H200
Const PD_RETURNDEFAULT = &H400
Const PD_SHOWHELP = &H800
Const PD_ENABLEPRINTHOOK = &H1000
Const PD_ENABLESETUPHOOK = &H2000
Const PD_ENABLEPRINTTEMPLATE = &H4000
Const PD_ENABLESETUPTEMPLATE = &H8000
Const PD_ENABLEPRINTTEMPLATEHANDLE = &H10000
Const PD_ENABLESETUPTEMPLATEHANDLE = &H20000
Const PD_USEDEVMODECOPIES = &H40000
Const PD_USEDEVMODECOPIESANDCOLLATE = &H40000
Const PD_DISABLEPRINTTOFILE = &H80000
Const PD_HIDEPRINTTOFILE = &H100000
Const PD_NONETWORKBUTTON = &H200000

Type udtPRINTDLG
    lStructSize As Long
    hwndOwner As Long
    hDevMode As Long
    hDevNames As Long
    hdc As Long
    flags As Long
    nFromPage As Integer
    nToPage As Integer
    nMinPage As Integer
    nMaxPage As Integer
    nCopies As Integer
    hInstance As Long
    lCustData As Long
    lpfnPrintHook As Long
    lpfnSetupHook As Long
    lpPrintTemplateName As String
    lpSetupTemplateName As String
    hPrintTemplate As Long
    hSetupTemplate As Long
End Type

Private Declare Function GetOpenFileName Lib "comdlg32.dll" Alias "GetOpenFileNameA" (lpOpenfilename As OPENFILENAME) As Long
Private Declare Function GetSaveFileName Lib "comdlg32.dll" Alias "GetSaveFileNameA" (lpOpenfilename As OPENFILENAME) As Long
Private Declare Function SHBrowseForFolder Lib "shell32.dll" Alias "SHBrowseForFolderA" (lpBrowseInfo As BROWSEINFO) As Long
Private Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" (ByVal pidl As Long, ByVal pszPath As String) As Long
Private Declare Function ChooseColor Lib "comdlg32.dll" Alias "ChooseColorA" (lpChoosecolor As udtCHOOSECOLOR) As Long
Private Declare Function ChooseFont Lib "comdlg32.dll" Alias "ChooseFontA" (lpChooseFont As udtCHOOSEFONT) As Long
Private Declare Function PrintDlg Lib "comdlg32.dll" Alias "PrintDlgA" (pPrintdlg As udtPRINTDLG) As Long

'Private Const LOGPIXELSX = 88   ' Logical pixels/inch in X
'Private Const LOGPIXELSY = 90   ' Logical pixels/inch in Y
'Private Declare Function GetDeviceCaps Lib "gdi32" (ByVal hdc As Long, ByVal nIndex As Long) As Long
'Private Declare Function MulDiv Lib "kernel32" (ByVal nNumber As Long, ByVal nNumerator As Long, ByVal nDenominator As Long) As Long

Public Function CommonDialogFileOpen(Optional hwndOwner As Long = 0, Optional strFilename As String = "", Optional strTitle As String = "打开文件", Optional strInitialPath As String = "", Optional strFilter As String = "所有文件") As String

    Dim ofn As OPENFILENAME
    Dim lngReturnValue As Long
    
    With ofn
        .lStructSize = Len(ofn)
        .hwndOwner = hwndOwner
        .hInstance = App.hInstance
        .lpstrFilter = Replace(strFilter, "|", vbNullChar) & vbNullChar & vbNullChar
        '.lpstrFile = Space(254)
        If Len(strFilename) > 254 Then
            .lpstrFile = strFilename
        Else
            .lpstrFile = strFilename & Space(254 - LenB(StrConv(strFilename, vbFromUnicode)))
        End If
        .nMaxFile = 255
        .lpstrFileTitle = Space(254)
        .nMaxFileTitle = 255
        .lpstrInitialDir = strInitialPath
        .lpstrTitle = strTitle
        '.nFilterIndex = 0
        '.lpstrDefExt = ".*"
        '.lStructSize = Len(ofn)
        .flags = 6148
    End With
    
    lngReturnValue = GetOpenFileName(ofn)
    
    If lngReturnValue >= 1 Then
        strFilename = Trim(ofn.lpstrFile)
        If Right(strFilename, 1) = vbNullChar Then strFilename = Left(strFilename, Len(strFilename) - 1)
        CommonDialogFileOpen = strFilename
    Else
        CommonDialogFileOpen = ""
    End If
    
End Function

Public Function CommonDialogFileSave(Optional hwndOwner As Long = 0, Optional strFilename As String = "", Optional strTitle As String = "保存文件", Optional strInitialPath As String = "", Optional strFilter As String = "所有文件") As String

    Dim ofn As OPENFILENAME
    Dim lngReturnValue As Long
    
    With ofn
        .lStructSize = Len(ofn)
        .hwndOwner = hwndOwner
        .hInstance = App.hInstance
        .lpstrFilter = Replace(strFilter, "|", vbNullChar) & vbNullChar & vbNullChar
        If Len(strFilename) > 254 Then
            .lpstrFile = strFilename
        Else
            .lpstrFile = strFilename & Space(254 - LenB(StrConv(strFilename, vbFromUnicode)))
        End If
        .nMaxFile = 255
        .lpstrFileTitle = Space(254)
        .nMaxFileTitle = 255
        .lpstrInitialDir = strInitialPath
        .lpstrTitle = strTitle
        '.nFilterIndex = 0
        '.lpstrDefExt = ".*"
        '.lStructSize = Len(ofn)
        .flags = 6148
    End With
    
    lngReturnValue = GetSaveFileName(ofn)
    
    If lngReturnValue >= 1 Then
        strFilename = Trim(ofn.lpstrFile)
        If Right(strFilename, 1) = vbNullChar Then strFilename = Left(strFilename, Len(strFilename) - 1)
        CommonDialogFileSave = strFilename
    Else
        CommonDialogFileSave = ""
    End If
    
End Function

Public Function CommonDialogGetFolder(Optional hwndOwner As Long = 0, Optional strTitle As String = "选择文件夹") As String

    Dim bi As BROWSEINFO
    Dim pidl As Long
    Dim strFolder As String
    
    strFolder = Space(255)
    
    With bi
        .hOwner = hwndOwner
        .ulFlags = BIF_RETURNONLYFSDIRS
        .pidlRoot = 0
        .lpszTitle = strTitle & vbNullChar
    End With
    
    pidl = SHBrowseForFolder(bi)
    
    If SHGetPathFromIDList(ByVal pidl, ByVal strFolder) Then
        strFolder = Trim(strFolder)
        If Right(strFolder, 1) = vbNullChar Then strFolder = Left(strFolder, Len(strFolder) - 1)
        CommonDialogGetFolder = strFolder
    Else
        CommonDialogGetFolder = ""
    End If
    
End Function

Public Function CommonDialogGetColor(Optional hwndOwner As Long = 0) As Long

    Dim lngReturnValue As Long
    Dim lpChoosecolor As udtCHOOSECOLOR
    Dim CustomColors() As Byte
    
    ' 初始化自定义颜色，4 个字节定义一种颜色，一共有 16 种颜色
    ReDim CustomColors(48) As Byte
    CustomColors(0) = 0
    CustomColors(1) = 0
    CustomColors(2) = 128
    CustomColors(3) = 0
    CustomColors(4) = 204
    CustomColors(5) = 230
    CustomColors(6) = 255
    CustomColors(7) = 0
    CustomColors(8) = 220
    CustomColors(9) = 246
    CustomColors(10) = 255
    CustomColors(11) = 0
    CustomColors(12) = 224
    CustomColors(13) = 238
    CustomColors(14) = 255
    CustomColors(15) = 0
    
    With lpChoosecolor
        .hwndOwner = hwndOwner
        .hInstance = App.hInstance
        .lpCustColors = StrConv(CustomColors, vbUnicode)
        .flags = 0
        .lStructSize = Len(lpChoosecolor)
    End With
    
    lngReturnValue = ChooseColor(lpChoosecolor)
    
    If lngReturnValue Then
        CommonDialogGetColor = lpChoosecolor.rgbResult
    Else
        CommonDialogGetColor = -1
    End If
    
End Function

Public Function CommonDialogGetFont(Optional hwndOwner As Long = 0, Optional FontName As String, Optional Size As Single, Optional Bold As Boolean, Optional Italic As Boolean, Optional Underline As Boolean, Optional StrikeOut As Boolean, Optional Color As Long) As Long

    Dim rc As Long
    Dim lpChooseFont As udtCHOOSEFONT
    Dim lpLogFont As udtLOGFONT
    
    ' 初始化缓存区
    With lpLogFont
        .lfFaceName = StrConv(FontName, vbFromUnicode) & vbNullChar
'        .lfHeight = -MulDiv(Size, GetDeviceCaps(hDC, LOGPIXELSY), 72)
        .lfHeight = Size * 4 / 3
        If Bold Then
            .lfWeight = 700
        Else
            .lfWeight = 400
        End If
        .lfItalic = Italic
        .lfUnderline = Underline
        .lfStrikeOut = StrikeOut
    End With
    
    ' 初始化字体结构
    With lpChooseFont
        .hInstance = App.hInstance
        .hwndOwner = hwndOwner
        .flags = CF_BOTH + CF_INITTOLOGFONTSTRUCT + CF_EFFECTS + CF_NOSCRIPTSEL
'        If IsNumeric(Size) Then .iPointSize = (Size * 10)
        If Bold Then .nFontType = .nFontType + BOLD_FONTTYPE
        If Italic Then .nFontType = .nFontType + ITALIC_FONTTYPE
        If IsNumeric(Color) Then .rgbColors = Color
        .lStructSize = Len(lpChooseFont)
        .lpLogFont = VarPtr(lpLogFont)
    End With
    
    rc = ChooseFont(lpChooseFont)
    
    If rc Then
        FontName = StrConv(lpLogFont.lfFaceName, vbUnicode)
        FontName = Left(FontName, InStr(FontName, vbNullChar) - 1)
        With lpChooseFont
            Size = .iPointSize / 10
            Bold = .nFontType And BOLD_FONTTYPE
            Italic = .nFontType And ITALIC_FONTTYPE
            Underline = lpLogFont.lfUnderline
            StrikeOut = lpLogFont.lfStrikeOut
        End With
        CommonDialogGetFont = rc
    Else
        CommonDialogGetFont = 0
    End If
    
End Function

Public Function CommonDialogShowPrinter(Optional hwndOwner As Long = 0) As Long

    Dim lngReturnValue As Long
    Dim lpPrintDlg As udtPRINTDLG
    
    With lpPrintDlg
        .hwndOwner = hwndOwner
        .hInstance = App.hInstance
        .flags = 0
'        .hdc
'        .hDevMode
'        .hDevNames
'        .hPrintTemplate
'        .hSetupTemplate
'        .nFromPage
'        .nToPage
'        .nCopies
'        .nMaxPage
'        .nMinPage
        .lStructSize = Len(lpPrintDlg)
    End With
    
    lngReturnValue = PrintDlg(lpPrintDlg)
    
    If lngReturnValue Then
        CommonDialogShowPrinter = lngReturnValue
    Else
        CommonDialogShowPrinter = 0
    End If
    
End Function

