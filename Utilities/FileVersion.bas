Attribute VB_Name = "FileVersionUtilities"
' 获取文件版本模块

Option Explicit

Private Declare Function GetFileVersionInfoSize Lib "version.dll" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long
Private Declare Function GetFileVersionInfo& Lib "version.dll" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwHandle As Long, ByVal dwLen As Long, lpData As Byte)

' 获取文件的版本号
Public Function GetFileVersion(strFilename As String)

    Dim dwHandle As Long, dwLen As Long
    Dim lpData() As Byte
    Dim strVersion As String
    
    dwLen = GetFileVersionInfoSize(strFilename, dwHandle)
    
    If dwLen > 0 Then
    
        ReDim lpData(dwLen + 1) As Byte
        
        Call GetFileVersionInfo(strFilename, dwHandle, dwLen, lpData(0))
        
        strVersion = StrConv(lpData, vbUnicode)
        
        If InStr(strVersion, "ProductVersion") > 0 Then
        
            GetFileVersion = Trim(Mid(strVersion, InStr(strVersion, "ProductVersion") + 16, 9))
            
        End If
        
    End If
    
End Function
